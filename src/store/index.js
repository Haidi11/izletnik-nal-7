import axios from 'axios'
import Vuex from 'vuex'
import Vue from 'vue'
Vue.use(Vuex)

/*const IzletModel = {
  naziv: String,
  lokacija: String,
  opis: String,
  datumOdhoda: Date,
  datumPrihoda: Date,
  cena: String
}*/

const state = {izleti: []}

const getters = {
  getContact: state => {
    return state.contact
  }
}

const actions = {
  getIzleti ({ commit }) {
    axios.get('http://localhost:3000/izleti',
      { headers:{}})
      .then(response => {
        commit('GET_IZLETI', response.data)
      })
  },

  createIzlet ({ commit }, izlet) {
    axios.post('http://localhost:3000/izleti', izlet,{
      headers: {}
    }).then(response => {
      commit('CREATE_IZLETI', response.data)
    })
  }
}

const mutations = {

  GET_IZLETI (state, izleti) {
    state.izleti = izleti
  },

  CREATE_IZLETI (state, contact) {
    state.contact = contact
  }
}


export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
